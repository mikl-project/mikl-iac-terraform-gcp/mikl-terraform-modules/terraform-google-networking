output "subnetwork_output" {
  value = google_compute_subnetwork.subnet-1.name
}

//by doing this subnet as output we can use subnet in any other vm config file/module
