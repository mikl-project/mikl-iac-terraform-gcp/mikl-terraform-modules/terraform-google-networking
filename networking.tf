# VPC creation

resource "google_compute_network" "mikl-vpc" {
  name                    = var.vpc_name
  auto_create_subnetworks = false
  mtu                     = 1460
  lifecycle {
    prevent_destroy = true
  }
  depends_on = [var.depends-on]
}





# subnet creation

resource "google_compute_subnetwork" "subnet-1" {
  name          = var.sub_network_name   
  ip_cidr_range = var.primay_ip_cidr_range
  region        = var.region
  network       = google_compute_network.mikl-vpc.id  # implict difference 
  private_ip_google_access = true
 
 # VM shouldn't have public ip because it's hosting private/internal application (don't expose to internet)
 /*
  secondary_ip_range {
    range_name    = "tf-secondary-range-update1"
    ip_cidr_range = var.secondary_ip_range  
  } */
  lifecycle {
    prevent_destroy = true
  }
}



#Firewall Creation

resource "google_compute_firewall" "private-ssh" {
  name    = var.firewall_name
  network = google_compute_network.mikl-vpc.id
  priority = 500
  direction = var.direction
 

  log_config {
    metadata = "INCLUDE_ALL_METADATA"
  }

  target_tags = var.target_tags  #list of strings

  allow {
    protocol = var.protocol
    ports    = var.ports  #list of strings
  }


  source_ranges = var.source_ranges  #list of strings
}
