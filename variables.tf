variable "vpc_name" {
type = string
}

variable "sub_network_name" {
type = string
}

variable "region" {
    type = string
}

variable "primay_ip_cidr_range" {
    type = string
}

/*
variable "secondary_ip_range" {
    type = string
}  */
variable "depends_on" {

}

variable "firewall_name" {
    type = string
}

variable "direction" {
    type = string
}

variable "target_tags" {
    type = list(string)
}

variable "protocol" {
    type = string
}

variable "ports" {
    type = list(string)
}

variable "source_ranges" {
    type = list(string)
}

